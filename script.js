"use strict";
$(document).ready(function() {

    var categoryInsertPoint = $('#new-ticket-form .row:nth-child(3)');
    var currentCategory = $('#id_category');
    var footerInsertPoint = $('#footer .col-md-6:first-child');

    category.change(function(){
        if (currentCategory.val()==3) {
            categoryInsertPoint.before('<div id="ppkInstructions" class="ppk-intro"><h2>Please read and follow these instructions carefully</h2><p>Platform Pack creation requests are considered a support request and are subject to the terms of your support agreement. Platform Pack creation <a href="http://localhost.smartdeploy.com/support/platform-packs/" target="_blank">fees</a> are based on the computer model and your current support level.</p><p>To complete the request, we need additional information about the computer for which you are requesting a Platform Pack. This involves running a script and completing the below form.</p><h3>Step 1: Download the VBS script</h3><p><a href="https://www.smartdeploy.com/files/wmi.vbs?version=1.2" target="_blank">Download the wmi.vbs script</a> and save to the computer for which you are requesting a Platform Pack.</p><h3>Step 2: Run the VBS script</h3><p>Double-click the wmi.vbs file on the computer you want a Platform Pack for which generates a wmi.txt file. Include the wmi.txt file in the form below.</p><h3>Step 3: Complete the form below</h3><p>Complete all required fields in the form below. The SmartDeploy Support team will create the Platform Pack for you and alert you when it is ready to download. Please allow 5-10 business days for creation of the Platform Pack.</p></div>')
        } else {
            $('#ppkInstructions').remove()
        }
    })

    footerInsertPoint.prepend('Technical, product or pricing questions? We\'re here to help.<br>888-7DEPLOY <a href="mailto:sales@smartdeploy.com?subject=Support Question">sales@smartdeploy.com</a>');

});
